const ask = (msg) => prompt(`${msg}`);

const reverString = (str) => str.split("").reverse().join("");

function writeToIFrame(str) {
    let container = document.getElementById("ej_three_container");
    let containerDoc = container.document;
    if (container.contentDocument) {
        containerDoc = container.contentDocument;
    } else {
        containerDoc = container.contentWindow.document;
    }
    containerDoc.open();
    containerDoc.write(str);
    containerDoc.close();
}

function findWordInParagraph() {
    let container = document.getElementById("ej_three_container");
    let containerDoc = container.document;
    if (container.contentDocument) {
        containerDoc = container.contentDocument;
    } else {
        containerDoc = container.contentWindow.document;
    }
    let textAreaContent = new String(document.getElementById("ej_three_textarea").value);
    let input = new String(document.getElementById("ej_three_word_to_search").value);

    containerDoc.open();
    if (textAreaContent.length >= 300) {

        if (textAreaContent.includes(input)) {
            containerDoc.writeln(`<h1><strong>${input}</strong>, si se encuentra dentro del parrafo</h1>`)
        } else {
            containerDoc.writeln(`<h1><strong>${input}</strong>, no se encuentra dentro del parrafo</h1>`)
        }
    } else {
        containerDoc.writeln(`<h1>El texto ingresado no tiene al menos 300 caracteres.</h1>`)
    }
    containerDoc.close();
}

function dummyEmailValidator() {
    let email = document.getElementById("email_field").value;
    let container = document.getElementById("ej_three_container");
    let containerDoc = container.document;
    if (container.contentDocument) {
        containerDoc = container.contentDocument;
    } else {
        containerDoc = container.contentWindow.document;
    }
    containerDoc.open();
    if (email.charAt(0) >= "0" && email.charAt(0) <= "9") {
        containerDoc.writeln("El correo comienza con un número<br>");
    } else {
        containerDoc.writeln("El correo comienza con una letra<br>");
    }
    if (email.includes("@")) {
        containerDoc.writeln("El corre contiene @<br>");
    } else {
        containerDoc.writeln("El corre no contiene @<br>");
    }
    if (email.endsWith(".com")) {
        containerDoc.writeln("El corre teriman con .com<br>");
    } else if (email.endsWith(".net")) {
        containerDoc.writeln("El corre teriman con .net<br>");
    } else if (email.endsWith(".mx")) {
        containerDoc.writeln("El corre teriman con .mx<br>");
    } else {
        containerDoc.writeln("El corre no tiene una terminación valida.");
    }

}

function isPalindrome() {
    let word = document.getElementById("text_field").value;
    let reversedWorkd = reverString(word);
    if (word === reversedWorkd) {
        writeToIFrame(`<h1>La palabra <em>${word}</em> es palindromo</h1>`);
    } else {
        writeToIFrame(`<h1>La palabra <em>${word}</em> no es palindromo</h1>`);
    }
}

function sentenceSpliter() {
    let sentencce = document.getElementById("text_field").value;
    let aCounter = 0;
    let container = document.getElementById("ej_three_container");
    let containerDoc = container.document;
    if (container.contentDocument) {
        containerDoc = container.contentDocument;
    } else {
        containerDoc = container.contentWindow.document;
    }
    containerDoc.open();
    containerDoc.writeln(`Ultima letra: ${sentencce.charAt(sentencce.length - 1)} <br>`);
    containerDoc.writeln("Caracteres: ");
    sentencce.split("").forEach((element, index) => {
        if (element === "a") { aCounter++; }
        if (index === sentencce.length - 1) {
            containerDoc.writeln(element)
        } else {
            containerDoc.writeln(`${element}.`)
        }
    });
    containerDoc.writeln(`<br>Cantidad de a's: ${aCounter} <br>`)
    containerDoc.writeln(`Invertido: ${reverString(sentencce)}<br>`);
    containerDoc.writeln(`Mitad: ${sentencce.split("", sentencce.length / 2).join("")}`);
}

function sentenceSorter() {
    let container = document.getElementById("ej_three_container");
    let containerDoc = container.document;
    if (container.contentDocument) {
        containerDoc = container.contentDocument;
    } else {
        containerDoc = container.contentWindow.document;
    }
    containerDoc.open();
    let sentence = document.getElementById("text_field").value
    sentence.split(" ")
        .sort((a, b) => a.localeCompare(b))
        .forEach((element) => {
            containerDoc.writeln(`${element}<br>`)
        });
}

function dummyPasswordValidator() {
    let password = document.getElementById("password_input").value;
    if (password.length < 8) {
        alert("La contraseña debe teenr al menos 8 caracteres");
    } else {
        let numbers = 0;
        let letter = 0;
        password.split("").forEach((element) => {
            if (isNaN(element)) {
                letter++;
            } else {
                numbers++;
            }
        });
        if (numbers < 1) {
            alert("La contraseña debe tener al menos un numero;");
        } else if (letter < 1) {
            alert("La contraseña debe tener al menos una letra");
        } else {
            writeToIFrame("Valido");
        }
    }
}

function colorSwitcher() {
    let d = new Date();
    let placeholder = document.getElementById("ej_three_container");
    let value = document.getElementById("color_switcher").value;
    let containerDoc = placeholder.document;
    if (placeholder.contentDocument) {
        containerDoc = placeholder.contentDocument;
    } else {
        containerDoc = placeholder.contentWindow.document;
    }
    placeholder.style.backgroundColor = value
    containerDoc.open();
    containerDoc.writeln(`RGB:${value} <br>Cambio: ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`);
}

function dateCalculator() {
    let initialDate = new Date(document.getElementById("initial_date").value);
    let finalDate = new Date(document.getElementById("final_date").value);

    writeToIFrame(`Han pasado ${finalDate.getMonth() - initialDate.getMonth()} meses y ${finalDate.getFullYear() - initialDate.getFullYear()} años`);
}